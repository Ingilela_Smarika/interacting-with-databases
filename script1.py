import psycopg2

def create_table():
    conn=psycopg2.connect("dbname='database1' user='postgres' password='smarikamaggie' host='localhost' port='5432'  ")
    cur=conn.cursor()
    cur.execute("CREATE TABLE IF NOT EXISTS store(item TEXT, quantity INTEGER,price REAL)")
    conn.commit()
    conn.close()

def insert(item,quantity,price):
    conn=psycopg2.connect("dbname='database1' user='postgres' password='smarikamaggie' host='localhost' port='5432' ")
    cur=conn.cursor()
    cur.execute("INSERT INTO store VALUES(%s,%s,%s)" %(item,quantity,price))
    conn.commit()
    conn.close()

def view():
    conn=psycopg2.connect("lite.db")
    cur=conn.cursor()
    cur.execute("SELECT * FROM store")
    rows=cur.fetchall()
    conn.close()
    return rows

def droptbl():
    conn=psycopg2.connect("lite.db")
    cur=conn.cursor()
    cur.execute("drop the table")
    rows=cur.fetchall()
    conn.close()
    return rows


create_table()


#delete("Wine Glass")
#update(11,6,"Wine Glass")
#print(view())

